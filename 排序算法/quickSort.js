//快排
function quickSort(list) {
    if (list.length == 0 || list.length == 1) {
        return list;
    }
    let center = Math.floor(list.length / 2);
    let currentItem = list.splice(center, 1);
    let leftList = [], rightList = [];
    // list.forEach(element => {
    //     if (element < currentItem) {
    //         leftList.push(element);
    //     } else {
    //         rightList.push(element);
    //     }

    // });
    
    list.forEach(ele => ele < currentItem ? leftList.push(ele) : rightList.push(ele));// 判断放到leftlist还是rightlist中
    console.log([leftList, currentItem, rightList]);
    return quickSort(leftList).concat(currentItem).concat(quickSort(rightList));

}

let list = [5, 3, 7, 86, 4, 9];

console.log(quickSort(list));