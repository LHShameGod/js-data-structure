function ArrayList() {
    this.array = []

    ArrayList.prototype.swap = (m, n) => {
        let temp = this.array[m]
        this.array[m] = this.array[n]
        this.array[n] = temp
    }

    ArrayList.prototype.insert = item => {
        this.array.push(item)
    }

    ArrayList.prototype.toString = () => {
        return this.array.join('-')
    }

    ArrayList.prototype.bubbleSort = () => {
        let length = this.array.length;

        for (let j = length - 1; j >= 0; j--) {
            for (let i = 0; i < j; i++) {
                //注意i<j就可以了
                //第一次进来，i=0
                //最后一次寄来 i=length-2,与j(length-1)进行比较

                if (this.array[i] > this.array[i + 1]) {
                    this.swap(i, i + 1)
                }
            }
        }
    }

}

let list = new ArrayList();
list.insert(2);
list.insert(5);
list.insert(1);
list.insert(4);
list.insert(3);
console.log(list.toString());
list.bubbleSort();
console.log(list.toString());

