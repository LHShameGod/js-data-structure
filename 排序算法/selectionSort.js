//选择排序减少了交换次数，但比较次数不变（高级冒泡）
//思路：选定第一个索引位置，然后和后面比较，当后面的数值小时，记录下此时的索引，然后以此索引为标志继续比下去
//      一个循环后，确定最小的值的位置，交换第一个索引和最小值索引，完成第一个循环，以此类推

function ArrayList() {
    this.array = []

    ArrayList.prototype.swap = (m, n) => {
        let temp = this.array[m]
        this.array[m] = this.array[n]
        this.array[n] = temp
    }

    ArrayList.prototype.insert = item => {
        this.array.push(item)
    }

    ArrayList.prototype.toString = () => {
        return this.array.join('-')
    }

    ArrayList.prototype.selectionSort = () => {
        let length = this.array.length;

        for (let i = 0; i < length; i++) {
            let temp = this.array[i]
            let slogin = i
            for (let j = i + 1; j < length; j++) {
                if (temp > this.array[j]) {
                    slogin = j
                    temp = this.array[j]
                }
            }
            this.swap(i, slogin)
        }
    }

}

let list = new ArrayList();
list.insert(2);
list.insert(5);
list.insert(1);
list.insert(4);
list.insert(3);
list.insert(500);

console.log(list.toString());
list.selectionSort();
console.log(list.toString());


