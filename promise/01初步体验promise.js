function rand(m, n) {
    return Math.ceil(Math.random() * (n - m + 1)) + m - 1
}

const p = new Promise((resolve, reject) => {
    setTimeout(() => {
        const n = rand(1, 100)
        n <= 30
            ? resolve(n) // 将promise对象的状态设置为成功
            : reject(n) // 将promise对象的状态设置为失败
    }, 1000);
})

p.then((value) => {
    console.log('成功，输出的数字是' + value)
}, (reason) => {
    console.log('失败！，输出的数字是' + reason)
})